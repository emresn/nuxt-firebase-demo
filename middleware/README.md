# MIDDLEWARE

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your application middleware.
Middleware let you define custom functions that can be run before rendering either a page or a group of pages.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/guide/routing#middleware).


// For Firebase JS SDK v7.20.0 and later, measurementId is optional

const firebaseConfig = {
  apiKey: "AIzaSyDJsN5Uf7avTmmpkCjnrz3Jj9hsS2Bns0E",
  authDomain: "todoapp-26cc2.firebaseapp.com",
  databaseURL: "https://todoapp-26cc2-default-rtdb.firebaseio.com",
  projectId: "todoapp-26cc2",
  storageBucket: "todoapp-26cc2.appspot.com",
  messagingSenderId: "957564137109",
  appId: "1:957564137109:web:7af77973b734dc4ade759c",
  measurementId: "G-1T3LDHP5V9"
};

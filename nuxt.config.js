export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "blogpage",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      {
        rel: "stylesheet",
        href:
          "https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css",
        integrity:
          "sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6",
        crossorigin: "anonymous"
      }
    ]
  },

  loading: {
    color: "blue",
    height: "5px"
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    // "~/assets/style/css/bootstrap.css"
    "~/assets/style/css/transition.css"
  ],

  env: {
    apiKey: "AIzaSyDJsN5Uf7avTmmpkCjnrz3Jj9hsS2Bns0E",
    authDomain: "todoapp-26cc2.firebaseapp.com",
    databaseURL: "https://todoapp-26cc2-default-rtdb.firebaseio.com/",
    projectId: "todoapp-26cc2",
    storageBucket: "todoapp-26cc2.appspot.com",
    messagingSenderId: "957564137109",
    appId: "1:957564137109:web:7af77973b734dc4ade759c",
    measurementId: "G-1T3LDHP5V9",
    firebaseAPIKEY: "AIzaSyDJsN5Uf7avTmmpkCjnrz3Jj9hsS2Bns0E"
  },

  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        path: "/custom-route",
        component: resolve(__dirname, "pages/test.vue")
      });
    }
  },
  transition: {
    name: "layout",
    mode: "out-in"
  },

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    // '@nuxtjs/eslint-module'
  ],
  serverMiddleware: ["~/api"],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    "@nuxtjs/axios"
  ],

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "en"
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {}
};

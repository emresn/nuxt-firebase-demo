export const axios = "axios";
import Cookie from "js-cookie";

export const state = () => ({
  authKey: null,
  localId: null
});

export const mutations = {
  setPosts(state, posts) {
    state.fetchedPosts = posts;
  },

  addPost(state, post) {
    state.fetchedPosts.push(post);
  },
  updatePost(state, editedPost) {
    let idx = state.fetchedPosts.findIndex(post => post.id == editedPost.id);
    if (idx) {
      state.fetchedPosts[idx] = editedPost;
    }
  },
  setAuthKey(state, value) {
    state.authKey = value;
  },
  setlocalId(state, value) {
    state.localId = value;
  },

  clearAuthKey(state) {
    Cookie.remove("authKey");
    Cookie.remove("expiresIn");
    Cookie.remove("localId");
    state.localId = null;
    state.authKey = null;
    if (process.client) {
      localStorage.removeItem("authKey");
      localStorage.removeItem("expiresIn");
      localStorage.removeItem("localId");
    }
  }
};

export const actions = {
  nuxtServerInit(vuexContext, context) {},

  addPost(vuexContext, post) {
    return this.$axios.post(
      process.env.databaseURL + vuexContext.state.localId + "/posts.json",
      post
    );
  },

  updatePost(vuexContext, editedPost) {
    return this.$axios.put(
      process.env.databaseURL +
        vuexContext.state.localId +
        "/posts/" +
        editedPost.id +
        ".json",
      editedPost
    );
  },

  initAuth(vuexContext, req) {
    let token;
    let expiresIn;
    let localId;
    if (req) {
      //// server side

      if (!req.headers.cookie) {
        return;
      }
      token = req.headers.cookie
        .split(";")
        .find(c => c.trim().startsWith("authKey"));

      localId = req.headers.cookie
        .split(";")
        .find(c => c.trim().startsWith("localId"));

      if (token) {
        token = token.split("=")[1];
      }
      if (token) {
        localId = localId.split("=")[1];
      }
      expiresIn = req.headers.cookie
        .split(";")
        .find(e => e.trim().startsWith("expiresIn="));
      if (expiresIn) {
        expiresIn = expiresIn.split("=")[1];
      }
    } else {
      ///client side
      // Client Üzerinde Calisiyoruz....
      token = localStorage.getItem("authKey");
      expiresIn = localStorage.getItem("expiresIn");
      localId = localStorage.getItem("localId");
    }

    if (new Date().getTime() > +expiresIn || !token) {
      vuexContext.commit("clearAuthKey");
    }

    vuexContext.commit("setAuthKey", token);
    vuexContext.commit("setlocalId", localId);
  },
  authUser(vuexContext, authData) {
    let authLink =
      "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=";
    if (authData.isUser) {
      authLink =
        "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=";
    }

    return this.$axios
      .post(authLink + process.env.firebaseAPIKEY, {
        email: authData.user.email,
        password: authData.user.password,
        returnSecureToken: true
      })
      .then(response => {
        let expiresIn = new Date().getTime() + +response.data.expiresIn * 1000; //3600

        Cookie.set("authKey", response.data.idToken);
        Cookie.set("localId", response.data.localId);
        Cookie.set("expiresIn", expiresIn);
        localStorage.setItem("authKey", response.data.idToken);
        localStorage.setItem("localId", response.data.localId);
        localStorage.setItem("expiresIn", expiresIn);
        vuexContext.commit("setAuthKey", response.data.idToken);
        vuexContext.commit("setlocalId", response.data.localId);
      });
  },
  logout(vuexContext) {
    vuexContext.commit("clearAuthKey");
  }
};

export const getters = {
  getPosts(state) {
    return state.fetchedPosts;
  },

  isAuthenticated(state) {
    return state.authKey != null;
  },
  getAuthKey(state) {
    return state.authKey;
  },
  getLocalId(state) {
    return state.localId;
  }
};
